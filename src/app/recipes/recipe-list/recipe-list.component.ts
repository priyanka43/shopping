//import { Component, OnInit ,EventEmitter,Output} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import{Recipe} from '../recipe.model';
import{RecipeService} from '../recipe.service';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
//@Output()recipeWasSelected=new EventEmitter<Recipe>();
recipes:Recipe[]
//=[
  //new Recipe('A Test Recipe',
  //'This is simply a Test',
  //'https://s8.favim.com/orig/150711/backgrounds-bacon-breakfast-brunch-Favim.com-2943938.jpg'),
  //new Recipe('A Test Recipe',
  //'This is simply a Test',
  //'https://www.ndtv.com/cooks/images/Vegetarian%20Khow%20Suey.jpg?downsize=650:400&output-quality=70&output-format=webp')
//];
 
  constructor(private recipeService:RecipeService) { }

  ngOnInit()
   {
    this.recipes=this.recipeService.getRecipes();
  }

 //onRecipeSelected(recipe:Recipe){
//this.recipeWasSelected.emit(recipe);
  //}
}
