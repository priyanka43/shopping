import { EventEmitter } from '@angular/core';
import{Recipe} from'./recipe.model';



export class RecipeService
{
    recipeSelected=new EventEmitter<Recipe>(); 
    private recipes:Recipe[]
    =[
        new Recipe('A Test Recipe',
        'This is simply a Test',
        'https://s8.favim.com/orig/150711/backgrounds-bacon-breakfast-brunch-Favim.com-2943938.jpg'),
        new Recipe('A Test Recipe',
        'This is simply a Test',
        'https://www.ndtv.com/cooks/images/Vegetarian%20Khow%20Suey.jpg?downsize=650:400&output-quality=70&output-format=webp')
      ]; 

      getRecipes(){
          return this.recipes.slice();
      }
}